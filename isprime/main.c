#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>

int main(int argc, char **argv) {
	if (argc != 2) {
		fputs("give me exactly 1 argument", stderr);
		return -1;
	}
	char *end;
	int64_t n = strtoll(argv[1], &end, 10);
	if (*end || n < 0) {
		fputs("malformed input", stderr);
		return -1;
	}
	
	for (uint32_t i = 2; (uint64_t) i * i <= (uint64_t) n; i++) {
		if (n % i == 0) {
			printf("%" PRId64 " is not prime (divisible by %" PRIu32 ")\n", n, i);
			return 0;
		}
	}
	printf("%" PRId64 " is prime\n", n);
	return 0;
}
