#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <vector>

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

void finish(int res) {
	printf("%d\n", res);
	exit(0);
}

std::vector<u32> prime_factor(u32 n) {
	std::vector<u32> res;
	for (u32 i = 2; (u64) i * i <= n; i++)
		while (n % i == 0) res.push_back(i), n /= i;
	if (n > 1) res.push_back(n);
	return res;
}

u32 mpow(u32 a, u32 b, u32 mod) {
	u32 res = 1;
	for (; b; b >>= 1) {
		if (b & 1) res = (u64) res * a % mod;
		a = (u64) a * a % mod;
	}
	return res;
}

bool is_proot(u32 x, u32 mod) {
	auto factors = prime_factor(mod - 1);
	for (auto i : factors) if (mpow(x, (mod - 1) / i, mod) == 1) return false;
	return true;
}

int main(int argc, char **argv) {
	if (argc != 2) {
		fputs("give me exactly 1 argument", stderr);
		return -1;
	}
	char *end;
	int32_t n = strtoll(argv[1], &end, 10);
	if (*end || n <= 0) {
		fputs("malformed input", stderr);
		return -1;
	}
	if (n == 1) finish(0);
	auto self_factors = prime_factor(n);
	if (self_factors.size() > 1) {
		printf("not a prime(divisible by %" PRId32 "\n", self_factors[0]);
		return 0;
	}
	
	for (u32 i = 1; i < (u32) n; i++) if (is_proot(i, n)) {
		printf("%d\n", i);
		return 0;
	}
	
	printf("FATAL : not found\n");
	return 0;
}
